Feature: Complete flow
  In order to validate the process
  As an PO
  I want complete the flow at the market

  @javascript
  Scenario: Go to the orders page
    Given I go to the home page
    When I click at the orders list link
    Then I should see the orders page
    And I should see my ordered product

  @javascript
  Scenario: Go to the checkout page
    Given I go to the orders page
    When I confirm my order
    Then I should see the checkout page

  @javascript
  Scenario: Go to the profile page
    Given I go to the checkout page
    When I confirm my address
    Then I should see my profile page

  @javascript
  Scenario: Include user name at the profile page
    Given I go to the home page
    And I type my name at the field
    When I complete the flow
    Then I should see my name at profile page
