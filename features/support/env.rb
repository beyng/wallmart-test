require 'capybara/cucumber'
require 'capybara-webkit'
require 'rspec/expectations'

# Headless setup
headless = nil

if ENV['HEADLESS'] == 'false'
  Capybara.javascript_driver = :selenium
else
  Capybara.javascript_driver = :webkit

  require 'headless'
  headless = Headless.new
  headless.start
end

# Capybara defaults
Capybara.default_wait_time = 10
