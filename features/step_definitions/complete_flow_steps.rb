Given /^I go to the home page$/ do
  visit 'http://localhost:3000'
end

Given /^I type my name at the field$/ do
  fill_in('user', with: 'Bruno Eyng')
end

When /^I click at the orders list link$/ do
  click_link 'Lista de produtos pedidos'
end

Then /^I should see the orders page$/ do
  expect(page).to have_content 'Meu carrinho'
end

Then /^I should see my ordered product$/ do
  expect(page).to have_content 'Lápis Faber Castel'
end

Given /^I go to the orders page$/ do
  step 'I go to the home page'
  step 'I click at the orders list link'
end

When /^I confirm my order$/ do
  click_link 'Finalizar pedido'
end

Then /^I should see the checkout page$/ do
  expect(page).to have_content 'Endereço de entrega'
end

Given /^I go to the checkout page$/ do
  step 'I go to the orders page'
  step 'I confirm my order'
end

When /^I confirm my address$/ do
  click_link 'Confirmar'
end

Then /^I should see my profile page$/ do
  expect(page).to have_content 'Histórico de Pedidos'
  expect(page).to have_content 'Meus Dados'
end

When /^I complete the flow$/ do
  expect(page).to have_content 'Olá Bruno Eyng'
  step 'I click at the orders list link'
  step 'I confirm my order'
  step 'I confirm my address'
end

Then /^I should see my name at profile page$/ do
  expect(page).to have_content 'Olá Bruno Eyng'
end
