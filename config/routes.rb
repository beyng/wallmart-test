Rails.application.routes.draw do
  get '/', to: 'welcome#index'

  get '/my-orders', to: 'orders#show', as: 'ordered_products_page'

  get '/checkout', to: 'orders#checkout', as: 'checkout_page'

  get '/user/profile', to: 'users#index', as: 'profile_page'
end
