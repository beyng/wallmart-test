(function() {
  'use strict'

  var welcome = new function() {
    var userContainer = $('.user-name'),

        init = function() {
          setUserName();
        },

        setUserName = function () {
          $('.confirm').on('click', function() {
            $.cookie('user_name', userContainer.find('input')[0].value);
          });
        };
    return {
      Init: init,
    };
  }();

  welcome.Init();
})();
